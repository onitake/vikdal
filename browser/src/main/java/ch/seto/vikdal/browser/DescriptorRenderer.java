package ch.seto.vikdal.browser;

import java.awt.Component;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

class DescriptorRenderer extends DefaultTreeCellRenderer {
	private static final long serialVersionUID = 1L;
	
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		Icon icon = iconForName(((DescriptorNode) value).getIcon());
		setLeafIcon(icon);
		setOpenIcon(icon);
		setClosedIcon(icon);
		return super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
	}

	public static Icon iconForName(String name) {
		if (name != null) {
			Icon imgIcon = null;
			URL url = DescriptorRenderer.class.getResource("/images/" + name + ".png");
			if (url != null) {
				imgIcon = new ImageIcon(url);
			}
			return imgIcon;
		}
		return null;
	}
}