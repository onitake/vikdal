package ch.seto.vikdal.dalvik;

import ch.seto.vikdal.java.SymbolTable;
import ch.seto.vikdal.java.transformers.StateTracker;
import japa.parser.ast.stmt.Statement;

public interface Instruction {
	/**
	 * The number of bytecode values occupied by this instruction. May depend on the arguments.<br/>
	 * <b>Note:</b> Subclasses that derive from {@link ch.seto.vikdal.dalvik.instructions.AbstractInstruction}
	 * don't need to override this method if the instruction size only depends on the opcode.
	 * The default implementation returns {@code this.getFormat().getInstructionSize()}.
	 * @return the instruction size in bytecode words (shorts)
	 */
	public int getInstructionSize();
	/**
	 * The bytecode format of this instruction.
	 * @return an instruction format
	 */
	public Format getFormat();
	/**
	 * The opcode this instruction represents, a value between 0x00 and 0xff. 
	 * @return the opcode of the instruction
	 */
	public int getOpcode();	
	/**
	 * The encoded bytecode sequence for this instruction.<br/>
	 * <b>Note:</b> Subclasses that derive from {@link ch.seto.vikdal.dalvik.instructions.AbstractInstruction}
	 * don't need to override this method.
	 * @return a bytecode sequence
	 */
	public short[] getBytecode();
	/**
	 * Decodes and assigns the arguments from a given bytecode sequence.<br/>
	 * The sequence needs to hold all the bytecodes for this instruction, starting from offset off,
	 * or an exception will be thrown.<br/>
	 * <b>Note:</b> Subclasses that derive from {@link ch.seto.vikdal.dalvik.instructions.AbstractInstruction}
	 * don't need to override this method.
	 * @param bytecode an array of bytecodes
	 * @param off the offset of the first bytecode to examine
	 * @throws ArrayIndexOutOfBoundsException if the bytecode sequence is too short
	 */
	public void setArguments(short[] bytecode, int off);
	/**
	 * Assigns the list of decoded but not interpreted (as long) arguments to this instruction.<br/>
	 * <b>Note:</b> Subclasses that derive from {@link ch.seto.vikdal.dalvik.instructions.AbstractInstruction}
	 * don't need to override this method if the instruction doesn't take arguments.
	 * @param args the list of instruction arguments
	 */
	public void setArguments(long[] args);
	/**
	 * Returns the list of interpreted (as long) but not encoded arguments of this instruction.<br/>
	 * <b>Note:</b> Subclasses that derive from {@link ch.seto.vikdal.dalvik.instructions.AbstractInstruction}
	 * don't need to override this method if the instruction doesn't take arguments.
	 * @return the list of instruction arguments
	 */
	public long[] getArguments();
	/**
	 * Checks if this instruction has one ore more branch targets.<br/>
	 * <b>Note:</b> Subclasses that derive from {@link ch.seto.vikdal.dalvik.instructions.AbstractInstruction}
	 * don't need to override this method if the instruction doesn't branch.
	 * @return true, if this is a branch instruction
	 */
	public boolean hasBranches();
	/**
	 * Returns a list of all branch targets for this instruction.<br/>
	 * <b>Note:</b> Subclasses that derive from {@link ch.seto.vikdal.dalvik.instructions.AbstractInstruction}
	 * don't need to override this method if the instruction doesn't branch.
	 * @return a list of branch labels (relative if {@link #areBranchesRelative()} returns true)
	 */
	public int[] getBranches();
	/**
	 * Checks if this instruction has one or more branch targets.<br/>
	 * <b>Note:</b> Subclasses that derive from {@link ch.seto.vikdal.dalvik.instructions.AbstractInstruction}
	 * don't need to override this method if the instruction doesn't branch, or branches are relative.
	 * @return true, if the branches from this instruction are relative
	 */
	public boolean areBranchesRelative();
	/**
	 * Checks if succeeding instructions can be reached directly, i.e. if there is a "next" instruction in the
	 * program flow. Branch targets do not count as "next" instructions. Return true if the instruction breaks
	 * flow, like in a return or unconditional goto statement.
	 * {@link ch.seto.vikdal.dalvik.instructions.AbstractInstruction} returns false by default.
	 */
	public boolean breaksProgramFlow();
	/**
	 * {@link ch.seto.vikdal.dalvik.instructions.AbstractInstruction} returns {@code toString()} by default.
	 * @param table a symbol table to retrieve data from
	 * @param tracker a register state tracker, may be updated
	 * @return a human-readable description of this instruction with added symbol names
	 */
	public String toString(SymbolTable table, StateTracker tracker);
	/**
	 * Transforms this instruction into an AST node.
	 * @param table a symbol table to retrieve data from
	 */
	public Statement toAST(SymbolTable table);
}
